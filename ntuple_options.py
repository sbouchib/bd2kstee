#For details on Upgrade MC params
#https://twiki.cern.ch/twiki/bin/view/LHCb/UpgradeMonteCarloSamples

from Configurables import DecayTreeTuple, DaVinci
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
# Define stripping lines we want to use
# evts/stripping lines of similar selections
#e.g. stripping28r2 --> collections of stream
#stream = 'AllStreams'
stream = 'Rec'


#https://twiki.cern.ch/twiki/bin/view/LHCb/UpgradeMonteCarloSamples
line = 'StrippingBu2LLKeeLine'
#line = 'B2LLXBDT_Bd2eeKstarLine'
#line = ''

dtt = DecayTreeTuple('TupleBd2Kstee')
dtt.Inputs=['/Event/{0}/Phys/{1}/Particles'.format(stream,line)]
#dtt.Decay = '[ Lambda_b0 -> p+ K- (J/psi(1S) -> e+ e-)]CC'
#dtt.
#"" : ""

#So the following job options are needed:
DaVinci().UserAlgorithms += [dtt]
DaVinci().Simulation   = True          
DaVinci().DataType = "Upgrade"
DaVinci().InputType = 'LDST'
DaVinci().TupleFile = '/eos/lhcb/user/s/sbouchib/Bd2Kstee/NTUPLES/DVntuple.root'
from Configurables import CondDB
CondDB().Upgrade = True
DaVinci().CondDBtag = "sim-20150716-vc-mu100"
DaVinci().DDDBtag = "dddb-20150702"

#As the trigger has not yet been run on these samples, the following settings are helpful:

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False


#dtt.setDescriptorTemplate('${Dstar}[D*(2010)+ -> ${D0}(D0 -> ${Km}K- ${Kp}K+) ${pisoft}pi+]CC')
#dtt.setDescriptorTemplate('${Lb}[Lambda_b0 -> ${L0}(Lambda(1520)0 -> ${p}p+ ${Km}K-) ${Jpsi}(J/psi(1S) -> ${E1}e+ ${E2}e-)]CC')
#dtt.Decay = '{[B0 -> e+ e- (K*(892)0 -> K+ pi-)]CC, [B~0 -> e- e+ (K*(892)~0 -> K- pi+)]CC}'
dtt.Decay = '[B0 -> e+ e- (K*(892)0 -> K+ pi-)]CC'


#New kinematics
track_tool = dtt.addTupleTool('TupleToolTrackInfo')
track_tool.Verbose = True
dtt.addTupleTool('TupleToolPrimaries')

IOHelper().inputFiles([
	'/eos/lhcb/user/s/sbouchib/Bd2Kstee/DST/00070697_00000003_2.ldst'	
], clear=True)
