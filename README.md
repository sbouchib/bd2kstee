# Bd2Kstee

This repo contains main scripts (DaVinci scripts, ganga job...) for the Upstream
electrons reconstruction project.

## Compilation

DaVinci:
* `LbLogin -c x86_64-slc6-gcc62-opt` 
* `lb-run DaVinci/v43r1 gaudirun.py ntuples_options.py` (otherwise, DDDB and DataType = 'Upgrade' not recognized)



## Locations
* (L)DST: `/eos/lhcb/user/s/sbouchib/Bd2Kstee/DST`
* NTuples: `/eos/lhcb/user/s/sbouchib/Bd2Kstee/NTUPLES`

