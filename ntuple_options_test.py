from Configurables import DecayTreeTuple, DaVinci
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
# Define stripping lines we want to use
# evts/stripping lines of similar selections
#e.g. stripping28r2 --> collections of stream
stream = 'AllStreams'
line = 'D2hhPromptDst2D2KKLine'

dtt = DecayTreeTuple('TupleDstToD0pi_D0ToKK')
dtt.Inputs=['/Event/{0}/Phys/{1}/Particles'.format(stream,line)]
#dtt.Decay = '[D*(2010)+ -> (D0 -> K- K+) pi+]CC'
dtt.setDescriptorTemplate('${Dstar}[D*(2010)+ -> ${D0}(D0 -> ${Km}K- ${Kp}K+) ${pisoft}pi+]CC')

#New kinematics
track_tool = dtt.addTupleTool('TupleToolTrackInfo')
track_tool.Verbose = True
dtt.addTupleTool('TupleToolPrimaries')
#lifetime info
dtt.D0.addTupleTool('TupleToolPropertime')

dstar_hybrid = dtt.Dstar.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_dstar')
d0_hybrid = dtt.D0.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_D0')
pisoft_hybrid = dtt.pisoft.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_pisoft')

preamble = ["DZ = VFASPF(VZ) - BPV(VZ)"]
dstar_hybrid.Preambulo = preamble
dstar_hybrid.Variables= {
  "mass" : "M",
  "dira" : "BPVDIRA",
  "max_pt" : "MAXTREE(ISBASIC & HASTRACK, PT, -1)",
  "dz" : "DZ"
}

d0_hybrid.Variables= {
  "mass" : "M"
}

pisoft_hybrid.Variables= {
  "P" : "P"
}

#decay tree fitter

dtt.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConsD')
dtt.Dstar.ConsD.constrainToOriginVertex = True
dtt.Dstar.ConsD.Verbose = True
dtt.Dstar.ConsD.daughtersToConstrain = ['D0']
dtt.Dstar.ConsD.UpdateDaughters = True

dtt.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConsD2Kpi')
dtt.Dstar.ConsD2Kpi.constrainToOriginVertex = True
dtt.Dstar.ConsD2Kpi.Verbose = True
dtt.Dstar.ConsD2Kpi.daughtersToConstrain = ['D0']
dtt.Dstar.ConsD2Kpi.UpdateDaughters = True

#substitue K+ to pi+
dtt.Dstar.ConsD2Kpi.Substitutions = {
  'Charm -> (D0 -> K- ^K+) Meson' : 'pi+',
  'Charm -> (D~0 -> K+ ^K-) Meson' : 'pi-'
}








DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000

DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
# can be found on dst.py file (or on bkpng browser)
DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-3' #only for simulation, for data automatically takes the latest

fltrs = LoKi_Filters(
	#STRIP_Code = "HLT_PASS_RE('StrippingD2hhPromptDst2DKKLineDecision')"
	STRIP_Code = "HLT_PASS_RE('StrippingD2hhPromptDst2D2KKLineDecision')"
)

DaVinci().EventPreFilters = fltrs.filters('Filters')


IOHelper().inputFiles([
	'./00070793_00000018_7.AllStreams.dst'
], clear=True)
